This project was created from the source code of the current version of JSON-java.
https://repo1.maven.org/maven2/org/json/json/20210307/json-20210307-sources.jar

It is NOT a fork of the current project on GitHub.
https://github.com/stleary/JSON-java

I did this so I could put a module-info.java file in the Netbeans Ant project.

This allows jlink to be used in any project that includes the new library.

Ant builds will skip jlink, if an automatic module library is included in the project.

The name of the module for the new library is 'org.json'.

The original license is retained.

It's here because I've used it in some of my other GitLab projects.
See: https://gitlab.com/LovesToCode/PeopleTest

Any changes to the original code should be done on the GitHub project listed above.
